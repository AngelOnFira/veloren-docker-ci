FROM ubuntu:latest

RUN apt-get update; export DEBIAN_FRONTEND=noninteractive; \
    apt-get install -y --no-install-recommends --assume-yes \
        build-essential \
        ca-certificates \
        cmake \
        curl \
        gcc \
        git \
        git-lfs \
        glib2.0 \
        libasound2-dev \
        libatk1.0-dev \
        libcairo2-dev \
        libclang-dev \
        libgtk-3-dev \
        libpango1.0-dev \
        libssl-dev \
        mingw-w64 \
        pkg-config \
        zip

RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y --default-toolchain nightly-2019-07-03; \
    echo "[target.x86_64-pc-windows-gnu]\nlinker = \"/usr/bin/x86_64-w64-mingw32-gcc\"" >> /root/.cargo/config; \
    mkdir -p /cache/veloren

WORKDIR /cache/veloren

RUN . /root/.cargo/env; \
    rustup target add x86_64-pc-windows-gnu; \
    rustup component add rustfmt-preview --toolchain=nightly-2019-07-03; \
    rustup component add clippy-preview --toolchain=nightly-2019-07-03; \
    rustup default nightly-2019-07-03
    #Remove tarpaulin install until it can be used. See issue #64
    #RUSTFLAGS="--cfg procmacro2_semver_exempt" cargo install cargo-tarpaulin;

RUN git clone https://gitlab.com/veloren/veloren.git; \
    cd veloren; \
    git lfs install; \
    git lfs fetch; \
    git lfs checkout

RUN . /root/.cargo/env; \
    cd veloren/voxygen; \
    cargo build; \
    cargo rustc

RUN . /root/.cargo/env; \
    cd veloren/server-cli; \
    cargo build; \
    cargo rustc

ENV PATH=/root/.cargo/bin:$PATH